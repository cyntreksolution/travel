@extends('layouts.master')

@section('content')
    <!-- start page title -->
    <div class="lc-page-title-wrap">
        <div class="lc-page-title lc-page-title-login-bg text-center rellax" data-rellax-speed="-4">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 rellax" data-rellax-speed="3">
                        <h2>Login/Register</h2>
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item"><a href="#">Login/Register</a></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="bg-single">
        <div class="lc-container">
            <div class="lc-single-page-top100">
                <!-- start login/Reg section -->
                <div class="lc-login-section">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 col-md-9 col-12">
                            <div class="lc-login-tab">
                                <div class="lc-registration-tab">
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item sign-in">
                                            <a class="nav-link" href="{{route('login')}}"><i class="fa fa-sign-in"></i>Sign in</a>
                                        </li>
                                        <li class="nav-item ">
                                            <a class="nav-link active" id="profile-tab" data-toggle="tab"
                                               href="#profile"
                                               role="tab" aria-controls="profile" aria-selected="false"><i
                                                    class="fa fa-list-alt"></i>Register</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="tab-content" id="myTabContent">
                                    <p class="text-center tab-content-title">Sign up Now</p>
                                    <div class="tab-pane fade show active" id="profile" role="tabpanel"
                                         aria-labelledby="profile-tab">
                                        <form method="POST" action="{{ route('register') }}">
                                            @csrf
                                            <div class="form-group">
                                                <label class="user-profile" for="forgetmail">Name</label>
                                                <input id="name" type="text"
                                                       class="form-control @error('name') is-invalid @enderror"
                                                       name="name" value="{{ old('name') }}" required
                                                       autocomplete="name" autofocus>
                                                @error('name')
                                                <span class="invalid-feedback"
                                                      role="alert"><strong>{{ $message }}</strong></span>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label class="user-profile" for="forgetpass2">Email Address</label>
                                                <input id="email" type="email"
                                                       class="form-control @error('email') is-invalid @enderror"
                                                       name="email" value="{{ old('email') }}" required
                                                       autocomplete="email">
                                                @error('email')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label class="user-profile" for="exampleInputPassword1">Password</label>
                                                <input id="password" type="password"
                                                       class="form-control @error('password') is-invalid @enderror"
                                                       name="password" required autocomplete="new-password">
                                                @error('password')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label class="user-profile" for="exampleInputPassword2"> Re-Type
                                                    Password</label>
                                                <input id="password-confirm" type="password" class="form-control"
                                                       name="password_confirmation" required
                                                       autocomplete="new-password">

                                            </div>
                                            <div class="form-group form-check">
                                                <input type="checkbox" class="form-check-input" id="forgetcheck1">
                                                <label class="form-check-label" for="forgetcheck1">Remember Me</label>
                                            </div>
                                            <button type="submit" class="btn btn-login">Sign Up</button>
                                            @if (Route::has('password.request'))
                                                <a class="forget-pass" href="{{ route('password.request') }}">
                                                    {{ __('Forgot Your Password?') }}
                                                </a>
                                            @endif
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end login/Reg section -->
            </div>
        </div>
    </div>
@endsection
