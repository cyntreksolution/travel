@extends('layouts.master')

@section('content')
    <!-- start page title -->
    <div class="lc-page-title-wrap">
        <div class="lc-page-title lc-page-title-login-bg text-center rellax" data-rellax-speed="-4">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 rellax" data-rellax-speed="3">
                        <h2>Login/Register</h2>
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item"><a href="#">Login/Register</a></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="bg-single">
        <div class="lc-container">
            <div class="lc-single-page-top100">
                <!-- start login/Reg section -->
                <div class="lc-login-section">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 col-md-9 col-12">
                            <div class="lc-login-tab">
                                <div class="lc-registration-tab">
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item sign-in">
                                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home"
                                               role="tab" aria-controls="home" aria-selected="true"><i
                                                    class="fa fa-sign-in"></i>Sign in</a>
                                        </li>
                                        <li class="nav-item sign-in">
                                            <a class="nav-link" href="{{route('register')}}"><i class="fa fa-list-alt"></i>Register</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="tab-content" id="myTabContent">
                                    <p class="text-center tab-content-title">Sign in into your Account</p>
                                    <div class="tab-pane fade show active" id="home" role="tabpanel"
                                         aria-labelledby="home-tab">
                                        <form method="POST" action="{{ route('login') }}">
                                            @csrf
                                            <div class="form-group">
                                                <label class="user-profile" for="signinmail">User Name</label>
                                                <input id="email" type="email"
                                                       class="form-control @error('email') is-invalid @enderror"
                                                       name="email" value="{{ old('email') }}" required
                                                       autocomplete="email" autofocus>
                                            </div>
                                            <div class="form-group">
                                                <label class="user-profile" for="signinpass">Password</label>
                                                <input id="password" type="password"
                                                       class="form-control @error('password') is-invalid @enderror"
                                                       name="password" required autocomplete="current-password">

                                            </div>
                                            <div class="form-group form-check">
                                                <input class="form-check-input" type="checkbox" name="remember"
                                                       id="remember" {{ old('remember') ? 'checked' : '' }}>
                                                <label class="form-check-label" for="remember">Remember Me</label>
                                            </div>
                                            <button type="submit" class="btn btn-login">Sign in</button>
                                            @if (Route::has('password.request'))
                                                <a class="forget-pass" href="{{ route('password.request') }}">
                                                    {{ __('Forgot Your Password?') }}
                                                </a>
                                            @endif
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end login/Reg section -->
            </div>
        </div>
    </div>
@endsection
