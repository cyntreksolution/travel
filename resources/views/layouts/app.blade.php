<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Traveler's Diary</title>

    <!-- Include CSS -->
    <link rel="stylesheet" href="{{asset('/css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('/css/themify-icons.css')}}">

    <!-- fonts -->

    <!-- main css -->
    <link rel="stylesheet" href="{{asset('css/mega/_mega.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

</head>
<body>

<section class="lcd-header-section">
    <!--sidebar start-->
    <aside>
        <div id="lcd-sidebar" class="nav-collapse">
            <!--logo start-->
            <div class="lcd-brand">
                <a href="/" class="logo">
                    <img src="{{asset('images/logo2.png')}}" alt="img">
                    <span>Travelers' Diary</span>
                </a>
                <div class="lcd-sidebar-toggle-box">
                    <span class="ti-menu"></span>
                </div>
            </div>
            <!--logo end-->
            <!-- sidebar menu start-->
            <div class="lcd-leftside-navigation">
                <div class="lcd-leftside-navigation-s">
                    <ul class="lcd-sidebar-menu" id="lcd-nav-accordion">
                        <li class="text-center border-top-0">
                            <div class="lcd-sidebar-profile">
                                <img src="{{asset('images/profile-photo.png')}}" alt="photo">
                                <h3 class="name">{{Auth::user()->name}}</h3>
                                <div class="profile-details">
                                    <a class="followers">Followers <span>1023</span></a>
                                    <a class="following">Following <span>103</span></a>
                                </div>
                            </div>
                        </li>
                        <li class="border-top-0">
                            <a class="active" href="#">
                                <i class="fa fa-user-o"></i>
                                <span class="menu-text">My Profile</span>
                            </a>
                        </li>
                        <li class="sub-menu">
                            <a href="javascript:;">
                                <i class="fa fa-save"></i>
                                <span class="menu-text">Pages</span>
                            </a>
                            <ul class="sub">
                                <li><a href="profile-blue.html"><i class="fa fa-angle-right"></i>Profile</a></li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>General</a></li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Listing</a></li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Review</a></li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Saved</a></li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Grids</a></li>
                            </ul>
                        </li>
                        <li class="sub-menu">
                            <a class="active" href="listing-blue.html">
                                <i class="fa fa-list-ul"></i>
                                <span class="menu-text">My Listing</span>
                            </a>
                        </li>
                        <li>
                            <a class="active" href="#">
                                <i class="fa fa-envelope-o"></i>
                                <span class="menu-text">Messages</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- sidebar menu end-->
        </div>
    </aside>
    <!--sidebar end-->
</section>

<!--main content start-->
<section id="lcd-main-content">

    <div class="lcd-main-content-area">
        <!-- LC navbar -->
        <nav id="lcdnavbar" class="navbar lcd-navbar fixed-top navbar-expand-lg">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="line"></span>
                <span class="line"></span>
                <span class="line"></span>
            </button>
            <div class="collapse navbar-collapse lc-menu" id="navbarSupportedContent">
                <div class="login-side">
                    <ul class="navbar-nav ml-auto">
                        <li><a class="btn btn-transparent ml-3" href=".#">+ Add Story</a>
                        <li><a class="btn btn-transparent ml-3" href=".#">+ Add Package</a>
                        <li><a class="btn btn-transparent ml-3" href=".#">+ Add Listing</a>
                        <li class="nav-item has-menu-item-submenu"><img src="{{asset('images/profile.png')}}"></li>
                        </li>
                        @if (Auth::check())
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
        @yield('content', 'Default Content')
    </div>
</section>
<!--main content end-->

<!-- Include JavaScript -->
<script src="{{asset('/js/jquery.min.js')}}"></script>
<script src="{{asset('/js/popper.min.js')}}"></script>
<script src="{{asset('/js/bootstrap.min.js')}}"></script>
<script src="{{asset('/js/jquery.dbjqaccordion.2.7.js')}}"></script>
<script src="{{asset('/js/jquery.inview.min.js')}}"></script>
<script src="{{asset('/js/chart.bundle.min.js')}}"></script>
<script src="{{asset('/js/chartjs-plugin-labels.min.js')}}"></script>
<script src="{{asset('/js/lcmega.js')}}"></script>

<!-- custom js -->
<script src="{{asset('js/admin-scripts.js')}}"></script>

</body>

