<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Travelers' Diary</title>
    <meta name="csrf-token" content="{{csrf_token()}}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('/css/ripple.min.css')}}">
    <link rel="stylesheet" href="{{asset('/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('/css/line-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('/css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('/css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('/css/leaflet.css')}}">
    <link rel="stylesheet" href="{{asset('/css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" href="{{asset('/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('/css/magnific-popup.css')}}">


    <!-- main css -->
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    @stack('styles')
</head>
<body>

<!-- LC navbar -->
<nav id="lcnavbar" class="navbar lc-navbar lc-navbar-xone fixed-top navbar-expand-lg">
    <div class="container">
        <a class="navbar-brand main-logo" href="/"><img src="{{asset('images/logo.png')}}" alt="logo"></a>
        <a class="navbar-brand mobile-logo" href="/"><img src="{{asset('images/logo.png')}}" alt="logo"> Listera</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="line"></span>
            <span class="line"></span>
            <span class="line"></span>
        </button>
        <div class="collapse navbar-collapse lc-menu" id="navbarSupportedContent">
            <ul class="navbar-nav ml-5">
                <li class="nav-item has-menu-item-megamenu"><a href="#">Home <i class="fa fa-angle-down"></i></a>
                    <div class="mega-menu">
                        <div class="container">
                            <!-- start widget -->
                            <div class="smm-widget-area">
                                <div class="row">
                                    <!-- single widget -->
                                    <div class="col-lg-3 col-md-6">
                                        <div class="widget smm-widget-list">
                                            <h4 class="smm-widget-title">Home Page 1</h4>
                                            <div class="thumb">
                                                <a href="gm-home1-blue.html">
                                                    <img src="images/home/1.png" alt="home one">
                                                </a>
                                                <span class="lc-h-btn">
                                                        <a class="gm-btn" href="gm-home1-blue.html">Google map</a>
                                                        <a class="osm-btn" href="osm-home1-blue.html">Open Street</a>
                                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- single widget -->
                                    <div class="col-lg-3 col-md-6">
                                        <div class="widget smm-widget-list">
                                            <h4 class="smm-widget-title">Home Page 2</h4>
                                            <div class="thumb">
                                                <a href="gm-home2-blue.html">
                                                    <img src="images/home/2.png" alt="home two">
                                                </a>
                                                <span class="lc-h-btn">
                                                        <a class="gm-btn" href="gm-home2-blue.html">Google map</a>
                                                        <a class="osm-btn" href="osm-home2-blue.html">Open Street</a>
                                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- single widget -->
                                    <div class="col-lg-3 col-md-6">
                                        <div class="widget smm-widget-list">
                                            <h4 class="smm-widget-title">Home Page 3</h4>
                                            <div class="thumb">
                                                <a href="gm-home3-blue.html">
                                                    <img src="images/home/3.png" alt="home three">
                                                </a>
                                                <span class="lc-h-btn">
                                                        <a class="gm-btn" href="gm-home3-blue.html">Google map</a>
                                                        <a class="osm-btn" href="osm-home3-blue.html">Open Street</a>
                                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- single widget -->
                                    <div class="col-lg-3 col-md-6">
                                        <div class="widget smm-widget-list">
                                            <h4 class="smm-widget-title">Home Page 4</h4>
                                            <div class="thumb">
                                                <a href="gm-home4-blue.html">
                                                    <img src="images/home/4.png" alt="home four">
                                                </a>
                                                <span class="lc-h-btn">
                                                        <a class="gm-btn" href="gm-home4-blue.html">Google map</a>
                                                        <a class="osm-btn" href="osm-home4-blue.html">Open Street</a>
                                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end widget -->
                        </div>
                    </div>
                </li>
                <li class="nav-item has-menu-item-submenu"><a href="#">Listing<i class="fa fa-angle-down"></i></a>
                    <ul class="sub-menu">
                        <li><a href="gm-add-listing-blue.html"><i class="fa fa-angle-right"></i>Add listing</a></li>
                        <li><a href="gm-listing-single-blue.html"><i class="fa fa-angle-right"></i>Listing single</a></li>
                        <li class="has-menu-item-submenu">
                            <a href="gm-searchmap-blue.html"><i class="fa fa-angle-right"></i>Search Map</a>
                            <ul class="sub-menu">
                                <li><a href="gm-searchmap-blue.html"><i class="fa fa-angle-right"></i>Style 1</a></li>
                                <li><a href="gm-searchmap-right-blue.html"><i class="fa fa-angle-right"></i>Style 2</a></li>
                                <li><a href="gm-searchmap-top-blue.html"><i class="fa fa-angle-right"></i>Style 3</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-menu-item-megamenu"><a href="#">Pages <i class="fa fa-angle-down"></i></a>
                    <div class="mega-menu">
                        <div class="container">
                            <!-- start widget -->
                            <div class="smm-widget-area">
                                <div class="row">
                                    <!-- single widget -->
                                    <div class="col-lg-3 col-md-6">
                                        <div class="widget smm-widget-list">
                                            <h4 class="smm-widget-title">Google Map</h4>
                                            <ul>
                                                <li><a href="gm-home1-blue.html"><i class="fa fa-angle-right"></i>Home - 1</a></li>
                                                <li><a href="gm-home2-blue.html"><i class="fa fa-angle-right"></i>Home - 2</a></li>
                                                <li><a href="gm-home3-blue.html"><i class="fa fa-angle-right"></i>Home - 3</a></li>
                                                <li><a href="gm-home4-blue.html"><i class="fa fa-angle-right"></i>Home - 4</a></li>
                                                <li><a href="gm-add-listing-blue.html"><i class="fa fa-angle-right"></i>Add listing</a></li>
                                                <li><a href="gm-author-profile-blue.html"><i class="fa fa-angle-right"></i>Author profile</a></li>
                                                <li><a href="gm-listing-single-blue.html"><i class="fa fa-angle-right"></i>Listing single</a></li>
                                                <li><a href="gm-searchmap-blue.html"><i class="fa fa-angle-right"></i>Search Map</a></li>
                                                <li><a href="gm-contact-blue.html"><i class="fa fa-angle-right"></i>Contact Page</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- single widget -->
                                    <div class="col-lg-3 col-md-6">
                                        <div class="widget smm-widget-list">
                                            <h4 class="smm-widget-title">Open Street Map</h4>
                                            <ul>
                                                <li><a href="osm-home1-blue.html"><i class="fa fa-angle-right"></i>Home - 1</a></li>
                                                <li><a href="osm-home2-blue.html"><i class="fa fa-angle-right"></i>Home - 2</a></li>
                                                <li><a href="osm-home3-blue.html"><i class="fa fa-angle-right"></i>Home - 3</a></li>
                                                <li><a href="osm-home4-blue.html"><i class="fa fa-angle-right"></i>Home - 4</a></li>
                                                <li><a href="osm-add-listing-blue.html"><i class="fa fa-angle-right"></i>Add listing</a></li>
                                                <li><a href="osm-author-profile-blue.html"><i class="fa fa-angle-right"></i>Author profile</a></li>
                                                <li><a href="osm-listing-single-blue.html"><i class="fa fa-angle-right"></i>Listing single</a></li>
                                                <li><a href="osm-searchmap-blue.html"><i class="fa fa-angle-right"></i>Search Map</a></li>
                                                <li><a href="osm-contact-blue.html"><i class="fa fa-angle-right"></i>Contact Page</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- single widget -->
                                    <div class="col-lg-3 col-md-6">
                                        <div class="widget smm-widget-list">
                                            <h4 class="smm-widget-title">Inner Pages</h4>
                                            <ul>
                                                <li><a href="about-blue.html"><i class="fa fa-angle-right"></i>About Us</a></li>
                                                <li><a href="blog-blue.html"><i class="fa fa-angle-right"></i>Blog</a></li>
                                                <li><a href="blog-details-blue.html"><i class="fa fa-angle-right"></i>Blog Details</a></li>
                                                <li><a href="how-it-works-blue.html"><i class="fa fa-angle-right"></i>How it works</a></li>
                                                <li><a href="pricing-blue.html"><i class="fa fa-angle-right"></i>pricing</a></li>
                                                <li><a href="login-blue.html"><i class="fa fa-angle-right"></i>Login</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- single widget -->
                                    <div class="col-lg-3 col-md-6">
                                        <div class="widget smm-widget-list">
                                            <h4 class="smm-widget-title">Admin Page</h4>
                                            <ul>
                                                <li><a href="admin/admin-blue.html"><i class="fa fa-angle-right"></i>Admin Home</a></li>
                                                <li><a href="admin/listing-blue.html"><i class="fa fa-angle-right"></i>Admin listing</a></li>
                                                <li><a href="admin/profile-blue.html"><i class="fa fa-angle-right"></i>Admin profile</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end widget -->
                        </div>
                    </div>
                </li>
                <!-- end Mega menu -->
            </ul>
        </div>
        <div class="login-side">
            <ul class="navbar-nav ml-auto">
                @if (Auth::check())
                    <li>
                    <a  href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                    </li>
                    @else
                    <li><a href="{{route('login')}}"><i class="fa fa-sign-in"></i>Log in</a></li>
                @endif
                <li><a class="btn btn-transparent ml-3" href="gm-add-listing-blue.html">+ Add Listing</a></li>
            </ul>
        </div>
    </div>
</nav>

@yield('content', 'Default Content')

<!-- start footer-section -->
<footer class="lc-footer-section">
    <div class="container">
        <div class="row justify-content-center">
            <!-- subscribtion -->
            <div class="col-12 col-md-10">
                <div class="lc-subscribtion text-center">
                    <h4 class="title">Join Our Newsletter
                        <i class="fa fa-paper-plane-o"></i>
                    </h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <div class="row justify-content-center">
                        <div class="col-12 col-md-8 col-lg-6">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Your Email Address" >
                                <div class="input-group-append">
                                    <button type="submit" class="input-group-text">Subscribe</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.subscribtion -->

            <!-- widget area -->
            <div class="col-12">
                <div class="widget-area">
                    <div class="row justify-content-center">
                        <!-- single widget star -->
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="widget widget_about">
                                <a href="/">
                                    <img src="images/icon/04.png" alt="logo">
                                    <span class="widget-title">Travelers' Dairy</span>
                                </a>
                                <p>Lorem ipsum dolor amet, conectetur the adipisicig elit, sed eiusmod tempor inciddunt labore dolore commodo consequat.</p>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="widget widget_links">
                                <div class="widget-title">
                                    <h4 class="title">Useful Links</h4>
                                </div>
                                <ul class="ul-link">
                                    <li><a href="#">Sign in</a></li>
                                    <li><a href="#">Sign Up</a></li>
                                    <li><a href="#">My Account</a></li>
                                    <li><a href="#">Add Listing</a></li>
                                    <li><a href="#">Price Table</a></li>
                                </ul>
                                <ul>
                                    <li><a href="#">Privacy Policy</a></li>
                                    <li><a href="#">F.A.Q</a></li>
                                    <li><a href="about-us.html">Our Pertners</a></li>
                                    <li><a href="contact.html">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="widget widget_feed">
                                <div class="widget-title">
                                    <h4 class="title">Instagram Feed</h4>
                                </div>
                                <ul class="feed-thumb">
                                    <li>
                                        <div class="thumb">
                                            <img src="images/instagram/01.png" alt="img">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="thumb">
                                            <img src="images/instagram/02.png" alt="img">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="thumb">
                                            <img src="images/instagram/03.png" alt="img">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="thumb">
                                            <img src="images/instagram/04.png" alt="img">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="thumb">
                                            <img src="images/instagram/05.png" alt="img">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="thumb">
                                            <img src="images/instagram/06.png" alt="img">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="thumb">
                                            <img src="images/instagram/07.png" alt="img">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="thumb">
                                            <img src="images/instagram/08.png" alt="img">
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div> <!-- /.row -->
                </div>
            </div>
            <!-- /.widget area -->

            <!-- widget info -->
            <div class="col-12">
                <div class="widget widget_info">
                    <ul>
                        <li><a href="#"><i class="fa fa-envelope-o"></i>travelersdiary@gmail.com</a></li>
                        <li><a href="#"><i class="fa fa-phone"></i>+94773315650</a></li>
                        <li><a href="#"><i class="fa fa-skype"></i>example.skype</a></li>
                    </ul>
                </div>
            </div>
            <!-- /.widget info -->
        </div>
    </div>
    <div class="lc-copy-right">
        <div class="container">
            <div class="row vertical-center-row">
                <div class="col-12 col-lg-5 col-md-12">
                    <div class="copyright">
                        <p>Powered By Cyntrek Solutions</p>
                    </div>
                </div>
                <div class="col-12 col-md-12 col-lg-3 offset-lg-3">
                    <ul class="social-area">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <a href="#" id="lc-toTop" class="lc-to-top"><i class="fa fa-angle-up"></i></a>
</footer>
<!-- end footer-section -->

<!-- popup content for all listing -->
<div class="modal fade lc-listing-modal" tabindex="-1" role="dialog" data-backdrop="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <!-- close button -->
            <button type="button" class="btn lc-button-close" data-dismiss="modal"><i class="la la-times"></i></button>
            <div class="modal-body">
                <div class="row no-gutters">
                    <div class="col-lg-7 order-lg-12 lc-popup-map-area">
                        <div class="lc-popup-map-view">
                            <div id="lc-popup-map"></div>
                        </div>
                    </div>
                    <div class="col-lg-5 order-lg-1 lc-popup-map-area">
                        <div class="lc-popup-listing-view"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.popup content -->


<!-- Optional  -->
<script src="{{asset('/js/jquery.min.js')}}"></script>
<script src="{{asset('/js/popper.min.js')}}"></script>
<script src="{{asset('/js/bootstrap.min.js')}}"></script>
<script src="{{asset('/js/lcmega.js')}}"></script>
<script src="{{asset('/js/imagesloaded.pkgd.min.js')}}"></script>
<script src="{{asset('/js/isotope.pkgd.min.js')}}"></script>
<script src="{{asset('/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('/js/waypoints.min.js')}}"></script>
<script src="{{asset('/js/jquery.counterup.min.js')}}"></script>
<script src="{{asset('/js/wow.min.js')}}"></script>
<script src="{{asset('/js/leaflet.js')}}"></script>
<script src="{{asset('/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('/js/select2.min.js')}}"></script>
<script src="{{asset('/js/leaflet.markercluster-src.js')}}"></script>
<script src="{{asset('/js/leaflet.layersupport.js')}}"></script>
<script src="{{asset('/js/jquery.magnific-popup.min.js')}}"></script>
{{--<script src="{{asset('/js/imageuploadify.js')}}"></script>--}}
<script src="{{asset('/js/rangeslider.min.js')}}"></script>

<!-- google map -->
<script src="{{asset('/js/leaflet-google.js')}}"></script>

<!-- custom js -->
<script src="{{asset('/js/map.js')}}"></script>
<script src="{{asset('/js/scripts.js')}}"></script>
@stack('scripts')
</body>
</html>
