@extends('layouts.app')

@section('content')
    <!-- lcd-panel-review section-->
    <div class="lcd-panel-review-top-section">
        <div class="row">
            <div class="col-xl-3 col-md-6">
                <div class="lcd-panel-review-single style1">
                    <div class="lcd-panel-review-head d-flex justify-content-between">
                        <h6>Total Post View</h6>
                        <i class="fa fa-eye"></i>
                    </div>
                    <div class="lcd-panel-review-body d-flex justify-content-between">
                        <i class="fa fa-signal"></i>
                        <span>165</span>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="lcd-panel-review-single style2">
                    <div class="lcd-panel-review-head d-flex justify-content-between">
                        <h6>Total Leads</h6>
                        <i class="fa fa-futbol-o"></i>
                    </div>
                    <div class="lcd-panel-review-body d-flex justify-content-between">
                        <i class="fa fa-signal"></i>
                        <span>27</span>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="lcd-panel-review-single style3">
                    <div class="lcd-panel-review-head d-flex justify-content-between">
                        <h6>Total Saved</h6>
                        <i class="fa fa-bookmark-o"></i>
                    </div>
                    <div class="lcd-panel-review-body d-flex justify-content-between">
                        <i class="fa fa-signal"></i>
                        <span>172</span>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="lcd-panel-review-single style4">
                    <div class="lcd-panel-review-head d-flex justify-content-between">
                        <h6>Total Reviews</h6>
                        <i class="fa fa-star"></i>
                    </div>
                    <div class="lcd-panel-review-body d-flex justify-content-between">
                        <i class="fa fa-signal"></i>
                        <span>135</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /End lcd-panel-review section-->

    <!-- lcd-panel-review section-->
    <div class="lcd-panel-review-section">
        <div class="row">
            <div class="col-xl-8 col-md-12 lcd-border-r lcd-nopadding-r">
                <div class="lcd-pending-header lcd-border-b">
                    <h2>Pending List</h2>
                </div>
                <div class="lcd-pending-details">
                    <div class="row">
                        <div class="col-xl-6 col-md-12 lcd-nopadding">
                            <div class="lcd-pending-details-info d-flex align-items-center media">
                                <img src="{{asset('images/pending_list/1.png')}}" alt="active image">
                                <div class="lcd-pending-details-content media-body">
                                    <h4>Alltime Best Burger House</h4>
                                    <ul>
                                        <li><i class="fa fa-hotel"></i> Hotel</li>
                                        <li><i class="fa fa-calendar"></i> 04 January 2018</li>
                                        <li><i class="fa fa-clock-o"></i> 23 Days Left</li>
                                        <li><i class="fa fa-check"></i> Pending</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-md-12 lcd-nopadding d-flex align-self-center">
                            <div class="lcd-pending-details-btn">
                                <ul>
                                    <li><a class="lcd-blue-btn" href="#">Change Plan</a></li>
                                    <li><a class="lcd-red-btn close" href="#"><i class="fa fa-close"></i> Remove</a></li>
                                    <li><a class="lcd-white-btn" href="#"><i class="fa fa-edit"></i> Edit</a></li>
                                    <li><a class="lcd-green-btn" href="#">Pay & Publish</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="lcd-pending-details">
                    <div class="row">
                        <div class="col-xl-6 col-md-12 lcd-nopadding">
                            <div class="lcd-pending-details-info d-flex align-items-center media">
                                <img src="{{asset('images/pending_list/2.png')}}" alt="active image">
                                <div class="lcd-pending-details-content media-body">
                                    <h4>The Museum of New York</h4>
                                    <ul>
                                        <li><i class="fa fa-hotel"></i> Hotel</li>
                                        <li><i class="fa fa-calendar"></i> 04 January 2018</li>
                                        <li><i class="fa fa-clock-o"></i> 23 Days Left</li>
                                        <li><i class="fa fa-check"></i> Pending</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-md-12 lcd-nopadding d-flex align-self-center">
                            <div class="lcd-pending-details-btn">
                                <ul>
                                    <li><a class="lcd-blue-btn" href="#">Change Plan</a></li>
                                    <li><a class="lcd-red-btn close" href="#"><i class="fa fa-close"></i> Remove</a></li>
                                    <li><a class="lcd-white-btn" href="#"><i class="fa fa-edit"></i> Edit</a></li>
                                    <li><a class="lcd-green-btn" href="#">Pay & Publish</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="lcd-pending-details">
                    <div class="row">
                        <div class="col-xl-6 col-md-12 lcd-nopadding">
                            <div class="lcd-pending-details-info d-flex align-items-center media">
                                <img src="{{asset('images/pending_list/3.png')}}" alt="active image">
                                <div class="lcd-pending-details-content media-body">
                                    <h4>Thai Traditional Massage</h4>
                                    <ul>
                                        <li><i class="fa fa-hotel"></i> Hotel</li>
                                        <li><i class="fa fa-calendar"></i> 04 January 2018</li>
                                        <li><i class="fa fa-clock-o"></i> 23 Days Left</li>
                                        <li><i class="fa fa-check"></i> Pending</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-md-12 lcd-nopadding d-flex align-self-center">
                            <div class="lcd-pending-details-btn">
                                <ul>
                                    <li><a class="lcd-blue-btn" href="#">Change Plan</a></li>
                                    <li><a class="lcd-red-btn close" href="#"><i class="fa fa-close"></i> Remove</a></li>
                                    <li><a class="lcd-white-btn" href="#"><i class="fa fa-edit"></i> Edit</a></li>
                                    <li><a class="lcd-green-btn" href="#">Pay & Publish</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="lcd-pending-details">
                    <div class="row">
                        <div class="col-xl-6 col-md-12 lcd-nopadding">
                            <div class="lcd-pending-details-info d-flex align-items-center media">
                                <img src="{{asset('images/pending_list/4.png')}}" alt="active image">
                                <div class="lcd-pending-details-content media-body">
                                    <h4>Great Bondi Beach Tour </h4>
                                    <ul>
                                        <li><i class="fa fa-hotel"></i> Hotel</li>
                                        <li><i class="fa fa-calendar"></i> 04 January 2018</li>
                                        <li><i class="fa fa-clock-o"></i> 23 Days Left</li>
                                        <li><i class="fa fa-check"></i> Pending</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-md-12 lcd-nopadding d-flex align-self-center">
                            <div class="lcd-pending-details-btn">
                                <ul>
                                    <li><a class="lcd-blue-btn" href="#">Change Plan</a></li>
                                    <li><a class="lcd-red-btn close" href="#"><i class="fa fa-close"></i> Remove</a></li>
                                    <li><a class="lcd-white-btn" href="#"><i class="fa fa-edit"></i> Edit</a></li>
                                    <li><a class="lcd-green-btn" href="#">Pay & Publish</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-12">
                <div class="lcd-recent-activities">
                    <div class="lcd-recent-activities-head">
                        <h5><i class="fa fa-bell-o"></i>Recent Activities</h5>
                    </div>
                    <div class="d-flex align-items-center media lcd-recent-activities-body">
                        <img src="{{asset('images/recent_active/1.png')}}" alt="active image">
                        <div class="media-body">
                            <h6>Mark Juck <span>Liked Your Post.</span></h6>
                            <p>7 Minute Ago</p>
                        </div>
                    </div>
                    <div class="d-flex align-items-center media lcd-recent-activities-body">
                        <img src="{{asset('images/recent_active/2.png')}}" alt="active image">
                        <div class="media-body">
                            <h6>Chis Maartin  <span>Left 5/5 Star Rating.</span></h6>
                            <p>13 Minute Ago</p>
                        </div>
                    </div>
                    <div class="d-flex align-items-center media lcd-recent-activities-body">
                        <img src="{{asset('images/recent_active/3.png')}}" alt="active image">
                        <div class="media-body">
                            <h6>Simon Chan <span>Send You Message.</span></h6>
                            <p>19 Minute Ago</p>
                        </div>
                    </div>
                    <div class="d-flex align-items-center media lcd-recent-activities-body">
                        <img src="{{asset('images/recent_active/4.png')}}" alt="active image">
                        <div class="media-body">
                            <h6>Cristi jeni <span>Added New Listing.</span></h6>
                            <p>26 Minute Ago</p>
                        </div>
                    </div>
                    <div class="d-flex align-items-center media lcd-recent-activities-body">
                        <img src="{{asset('images/recent_active/5.png')}}" alt="active image">
                        <div class="media-body">
                            <h6>Jenifer Lucas <span>Write a New Review.</span></h6>
                            <p>31 Minute Ago</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
