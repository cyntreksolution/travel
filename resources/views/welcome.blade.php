@extends('layouts.master')

@section('content')
    <!-- banner start -->
    <div class="lc-banner lc-banner-one bg-one">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6 col-12 offset-lg-1 order-lg-12">
                    <div class="lc-fake-notification">
                        <div class="lc-fake-notification-thumb">
                            <img class="lc-fake-map-img" src="images/map-pattern.png" alt="image">
                        </div>
                        <div class="lc-msg">
                            <img src="images/popup-image/01.png" alt="logo">
                            <div class="popup"></div>
                        </div>
                        <div class="lc-msg">
                            <img src="images/popup-image/02.png" alt="logo">
                            <div class="popup"></div>
                        </div>
                        <div class="lc-msg">
                            <img src="images/popup-image/03.png" alt="logo">
                            <div class="popup"></div>
                        </div>
                        <div class="lc-msg">
                            <img src="images/popup-image/04.png" alt="logo">
                            <div class="popup"></div>
                        </div>
                        <div class="lc-msg">
                            <img src="images/popup-image/05.png" alt="logo">
                            <div class="popup"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-8 col-12 order-lg-1">
                    <h2 class="entry-title">Find the best places or clicking on the suggestions below</h2>
                    <h2 class="entry-title">Find the best places or clicking on the suggestions below</h2>
                    <h2 class="entry-title">Find the best places or clicking on the suggestions below</h2>
{{--                    <div class="lc-cat-browse">--}}
{{--                        <a href="#" class="cat-single wow animated fadeIn" data-wow-duration="0.1s" data-wow-delay="0.1s">--}}
{{--                            <img src="images/icon/banner_cat_01.png" alt="icon">--}}
{{--                            <span>Night Life</span>--}}
{{--                        </a>--}}
{{--                        <a href="#" class="cat-single wow animated fadeIn" data-wow-duration="0.2s" data-wow-delay="0.2s">--}}
{{--                            <img src="images/icon/05.png" alt="icon">--}}
{{--                            <span>Hotels</span>--}}
{{--                        </a>--}}
{{--                        <a href="#" class="cat-single wow animated fadeIn" data-wow-duration="0.3s" data-wow-delay="0.3s">--}}
{{--                            <img src="images/icon/06.png" alt="icon">--}}
{{--                            <span>Restaurent</span>--}}
{{--                        </a>--}}
{{--                        <a href="#" class="cat-single wow animated fadeIn" data-wow-duration="0.4s" data-wow-delay="0.4s">--}}
{{--                            <img src="images/icon/07.png" alt="icon">--}}
{{--                            <span>Shopping</span>--}}
{{--                        </a>--}}
{{--                        <a href="#" class="cat-single wow animated fadeIn" data-wow-duration="0.5s" data-wow-delay="0.5s">--}}
{{--                            <img src="images/icon/08.png" alt="icon">--}}
{{--                            <span>Coffee</span>--}}
{{--                        </a>--}}
{{--                        <a href="#" class="cat-single wow animated fadeIn" data-wow-duration="0.6s" data-wow-delay="0.6s">--}}
{{--                            <img src="images/icon/09.png" alt="icon">--}}
{{--                            <span>Medicale</span>--}}
{{--                        </a>--}}
{{--                    </div>--}}
                </div>
                <div class="col-lg-10 order-lg-12">
                    <div class="lc-main-search-tab">
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-place-tab" data-toggle="pill" href="#pills-place" role="tab" aria-controls="pills-place" aria-selected="true"><i class="fa fa-map-o"></i>Place</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-events-tab" data-toggle="pill" href="#pills-events" role="tab" aria-controls="pills-events" aria-selected="false"><i class="fa fa-calendar-plus-o"></i>Events</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-place" role="tabpanel" aria-labelledby="pills-place-tab">
                                <!-- dropdown search form start -->
                                <form class="lc-main-search" method="GET">
                                    <div class="lc-main-search-field">
                                        <input type="text" class="form-control" placeholder="What are you looking for">
                                    </div>
                                    <div class="lc-search-location">
                                        <input type="text" class="form-control" placeholder="Location">
                                    </div>
                                    <div class="lg-mulitsearch-cat">
                                        <div class="styled-input">
                                            <input id="category_search" class="form-control" placeholder="All Category" type="text"/>
                                            <ul class="category-dropdown">
                                                <li><a href="#hotel">Hotel</a></li>
                                                <li><a href="#shop">Shop</a></li>
                                                <li><a href="#events">Events</a></li>
                                                <li><a href="#spa">Spa</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <button class="btn btn-default"><i class="fa fa-search"></i> Search</button>
                                    <div class="fix"></div>
                                </form>
                                <!-- End dropdown search form -->
                            </div>
                            <div class="tab-pane fade" id="pills-events" role="tabpanel" aria-labelledby="pills-events-tab">
                                <!-- dropdown search form start -->
                                <form class="lc-main-search" method="GET">
                                    <div class="lc-main-search-field">
                                        <input type="text" class="form-control" placeholder="What are you looking for">
                                    </div>
                                    <div class="lc-search-location">
                                        <input type="text" class="form-control" placeholder="Location">
                                    </div>
                                    <div class="lg-mulitsearch-cat">
                                        <div class="styled-input">
                                            <input id="category_search_two" class="form-control" placeholder="All Category" type="text"/>
                                            <ul class="category-dropdown-two">
                                                <li><a href="#hotel">Hotel</a></li>
                                                <li><a href="#shop">Shop</a></li>
                                                <li><a href="#events">Events</a></li>
                                                <li><a href="#spa">Spa</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <button class="btn btn-default"><i class="fa fa-search"></i> Search</button>
                                    <div class="fix"></div>
                                </form>
                                <!-- End dropdown search form -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- banner end -->

    <!-- start feature section -->
    <div class="section lc-feature-listing bg-gray pd-default">
        <div class="container">
            <div class="row justify-content-center">
                <!-- section title -->
                <div class="col-12">
                    <div class="lc-section-title">
                        <h1 class="entry-title">Featured Packages</h1>
                        <div class="shadow-text">Featured Packages</div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing</p>
                        <a class="btn btn-listing" href="#">All Packages</a>
                    </div>
                </div>
                <!-- single listing -->
                <div class="col-12 col-lg-4 col-md-6 wow animated fadeIn" data-wow-duration="0.1s" data-wow-delay="0.1s">
                    <div class="lc-listing-single" data-lat="43.64701" data-lng="-79.39425" data-maptype="gm" data-marker="images/marker/spa.png">
                        <div class="entry-thumb">
                            <img class="img-thumbnail" src="images/features/01.png"  alt="img">
                            <span class="btn-close">Close</span>
                            <div class="entry-thumb-content">
                                <span class="rating">
                                    <i class="fa fa-star"></i>5.0
                                    <span class="count">(54)</span>
                                </span>
                                <span class="float-right">
                                    <i class="fa fa-bookmark-o"></i>
                                    <a class="btn btn-primary" data-toggle="modal" data-target=".lc-listing-modal">
                                       <span><i class="fa fa-eye"></i></span>
                                    </a>
                                </span>
                            </div>
                        </div>
                        <div class="entry-content">
                            <div class="media">
                                <div class="media-body">
                                    <h4><a class="title" href="#">Kennedy Space Center</a></h4>
                                    <span><i class="fa fa-bed"></i><a href="#">Hotel</a></span>
                                    <span><i class="fa fa-map-marker"></i><a href="#">Hilton, New Jersey</a></span>
                                </div>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consctetur pising elit, sed eiusmod tempor incididunt labore et dolore magna aliqua.</p>
                        </div>
                        <div class="entry-footer">
                            <span>$150 - $750</span>
                            <span class="right">+44 20 7336</span>
                        </div>
                    </div>
                </div>
                <!-- /. single listing -->

                <!-- single listing -->
                <div class="col-12 col-lg-4 col-md-6 wow animated fadeIn" data-wow-duration="0.2s" data-wow-delay="0.2s">
                    <div class="lc-listing-single" data-lat="-37.8210922667" data-lng="175.2209316333" data-maptype="gm" data-marker="images/marker/resturent.png">
                        <div class="entry-thumb">
                            <img class="img-thumbnail" src="images/features/02.png"  alt="img">
                            <span class="btn-close">Open</span>
                            <div class="entry-thumb-content">
                                <span class="rating">
                                    <i class="fa fa-star"></i>4.9
                                    <span class="count">(79)</span>
                                </span>
                                <span class="float-right">
                                    <i class="fa fa-bookmark-o"></i>
                                    <a class="btn btn-primary" data-toggle="modal" data-target=".lc-listing-modal">
                                       <span><i class="fa fa-eye"></i></span>
                                    </a>
                                </span>
                            </div>
                        </div>
                        <div class="entry-content">
                            <div class="media">
                                <div class="media-body">
                                    <h4><a class="title" href="#">The Burger House</a></h4>
                                    <span><i class="fa fa-cutlery"></i><a href="#">Restaurent</a></span>
                                    <span><i class="fa fa-map-marker"></i><a href="#">New Jersey</a></span>
                                </div>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consctetur pising elit, sed eiusmod tempor incididunt labore et dolore magna aliqua.</p>
                        </div>
                        <div class="entry-footer">
                            <span>$10 - $200</span>
                            <span class="right">+44 20 7336</span>
                        </div>
                    </div>
                </div>
                <!-- /. single listing -->

                <!-- single listing -->
                <div class="col-12 col-lg-4 col-md-6 wow animated fadeIn" data-wow-duration="0.3s" data-wow-delay="0.3s">
                    <div class="lc-listing-single" data-lat="-37.8210881833" data-lng="175.2215004833" data-maptype="gm" data-marker="images/marker/vintage.png">
                        <div class="entry-thumb">
                            <img class="img-thumbnail" src="images/features/03.png"  alt="img">
                            <span class="btn-close">Open</span>
                            <div class="entry-thumb-content">
                                <span class="rating">
                                    <i class="fa fa-star"></i>4.9
                                    <span class="count">(79)</span>
                                </span>
                                <span class="float-right">
                                    <i class="fa fa-bookmark-o"></i>
                                    <a class="btn btn-primary" data-toggle="modal" data-target=".lc-listing-modal">
                                       <span><i class="fa fa-eye"></i></span>
                                    </a>
                                </span>
                            </div>
                        </div>
                        <div class="entry-content">
                            <div class="media">
                                <div class="media-body">
                                    <h4><a class="title" href="#">Vintage</a></h4>
                                    <span><i class="fa fa-glass"></i><a href="#">Night Life</a></span>
                                    <span><i class="fa fa-map-marker"></i><a href="#">New Jersey</a></span>
                                </div>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consctetur pising elit, sed eiusmod tempor incididunt labore et dolore magna aliqua.</p>
                        </div>
                        <div class="entry-footer">
                            <span>$25 - $575</span>
                            <span class="right">+44 20 7336</span>
                        </div>
                    </div>
                </div>
                <!-- /. single listing -->
            </div>
        </div>
    </div>
    <!-- end feature section -->

    <!-- start work section -->
    <div class="section lc-how-works pd-default-2">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5 col-md-12">
                    <div class="lc-section-title">
                        <h1 class="entry-title">How It Work</h1>
                        <div class="shadow-text">FeatureS</div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim for the veniam, quis nostrud to exercitation ullamco beautifull laboris.</p>
                    </div>
                </div>
                <div class="col-lg-3 offset-xl-1 col-md-5 offset-md-1">
                    <div class="lc-intro-box">
                        <div class="icon">
                            <img src="images/icon/01.png" alt="img">
                            <span class="hide-text">01.</span>
                        </div>
                        <h4 class="title">Find Place</h4>
                        <p>Lorem ipsum to dolor for amet sectetur adipising teiusmod to tempor incidiedunt labore et dolore magna aliqua.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-5">
                    <div class="lc-intro-box">
                        <div class="icon">
                            <img src="images/icon/02.png" alt="img">
                            <span class="hide-text">02.</span>
                        </div>
                        <h4 class="title">Contact Owners</h4>
                        <p>Lorem ipsum to dolor for amet sectetur adipising teiusmod to tempor incidiedunt labore et dolore magna aliqua.</p>
                    </div>
                    <div class="lc-intro-box">
                        <div class="icon">
                            <img src="images/icon/03.png" alt="img">
                            <span class="hide-text">03.</span>
                        </div>
                        <h4 class="title">Reservation</h4>
                        <p>Lorem ipsum to dolor for amet sectetur adipising teiusmod to tempor incidiedunt labore et dolore magna aliqua.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end work section -->

    <!-- start explore section -->
    <div class="section lc-explore-listing pd-default bg-gray">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="lc-section-title">
                        <h1 class="entry-title">Explore By City</h1>
                        <div class="shadow-text">CITIeS</div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing</p>
                        <a class="btn btn-listing" href="#">More Cities</a>
                    </div>
                </div>
                <div class="col-12">
                    <div class="lc-gallery-wrap">
                        <!-- item -->
                        <div class="lc-city-listing lc-gallery-item lc-gallery-item-small">
                            <div class="entry-thumb">
                                <div class="thumb-overlay"></div>
                                <a href="#" class="overlay-link"></a>
                                <img src="images/features/city/01.png" alt="img">
                                <!-- hover content -->
                                <div class="entry-content">
                                    <h4 class="meta">New York</h4>
                                    <p class="post-count">7 Listing</p>
                                    <div class="entry-author">
                                        <ul class="author-list">
                                            <li><a href="#"><img src="images/author/01.png" alt="thumb"></a></li>
                                            <li><a href="#"><img src="images/author/02.png" alt="thumb"></a></li>
                                            <li>+7</li>
                                        </ul>
                                        <a href="#" class="right-arrow"><i class="la la-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- /.lc-city-listing -->
                        <!-- item -->
                        <div class="lc-city-listing lc-gallery-item lc-gallery-item-large">
                            <div class="entry-thumb">
                                <div class="thumb-overlay"></div>
                                <a href="#" class="overlay-link"></a>
                                <img src="images/features/city/02.png" alt="img">
                                <!-- hover content -->
                                <div class="entry-content">
                                    <h4 class="meta">Las Vegas</h4>
                                    <p class="post-count">8 Listing</p>
                                    <div class="entry-author">
                                        <ul class="author-list">
                                            <li><a href="#"><img src="images/author/01.png" alt="thumb"></a></li>
                                            <li><a href="#"><img src="images/author/02.png" alt="thumb"></a></li>
                                            <li>+7</li>
                                        </ul>
                                        <a href="#" class="right-arrow"><i class="la la-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- /.lc-city-listing -->
                        <!-- item -->
                        <div class="lc-city-listing lc-gallery-item lc-gallery-item-small">
                            <div class="entry-thumb">
                                <div class="thumb-overlay"></div>
                                <a href="#" class="overlay-link"></a>
                                <img src="images/features/city/03.png" alt="img">
                                <!-- hover content -->
                                <div class="entry-content">
                                    <h4 class="meta">Louisiana</h4>
                                    <p class="post-count">2 Listing</p>
                                    <div class="entry-author">
                                        <ul class="author-list">
                                            <li><a href="#"><img src="images/author/01.png" alt="thumb"></a></li>
                                            <li><a href="#"><img src="images/author/02.png" alt="thumb"></a></li>
                                            <li>+7</li>
                                        </ul>
                                        <a href="#" class="right-arrow"><i class="la la-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- /.lc-city-listing -->

                        <!-- item -->
                        <div class="lc-city-listing lc-gallery-item lc-gallery-item-small">
                            <div class="entry-thumb">
                                <div class="thumb-overlay"></div>
                                <a href="#" class="overlay-link"></a>
                                <img src="images/features/city/05.png" alt="img">
                                <!-- hover content -->
                                <div class="entry-content">
                                    <h4 class="meta">North Carolina</h4>
                                    <p class="post-count">1 Listing</p>
                                    <div class="entry-author">
                                        <ul class="author-list">
                                            <li><a href="#"><img src="images/author/01.png" alt="thumb"></a></li>
                                            <li><a href="#"><img src="images/author/02.png" alt="thumb"></a></li>
                                            <li>+7</li>
                                        </ul>
                                        <a href="#" class="right-arrow"><i class="la la-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- /.lc-city-listing -->
                        <!-- item -->
                        <div class="lc-city-listing lc-gallery-item lc-gallery-item-large">
                            <div class="entry-thumb">
                                <div class="thumb-overlay"></div>
                                <a href="#" class="overlay-link"></a>
                                <img src="images/features/city/04.png" alt="img">
                                <!-- hover content -->
                                <div class="entry-content">
                                    <h4 class="meta">Alabama</h4>
                                    <p class="post-count">7 Listing</p>
                                    <div class="entry-author">
                                        <ul class="author-list">
                                            <li><a href="#"><img src="images/author/01.png" alt="thumb"></a></li>
                                            <li><a href="#"><img src="images/author/02.png" alt="thumb"></a></li>
                                            <li>+7</li>
                                        </ul>
                                        <a href="#" class="right-arrow"><i class="la la-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- /.lc-city-listing -->
                        <!-- item -->
                        <div class="lc-city-listing lc-gallery-item lc-gallery-item-small lc-gallery-item-991">
                            <div class="entry-thumb">
                                <div class="thumb-overlay"></div>
                                <a href="#" class="overlay-link"></a>
                                <img src="images/features/city/06.png" alt="img">
                                <!-- hover content -->
                                <div class="entry-content">
                                    <h4 class="meta">Oklahama</h4>
                                    <p class="post-count">2 Listing</p>
                                    <div class="entry-author">
                                        <ul class="author-list">
                                            <li><a href="#"><img src="images/author/01.png" alt="thumb"></a></li>
                                            <li><a href="#"><img src="images/author/02.png" alt="thumb"></a></li>
                                            <li>+7</li>
                                        </ul>
                                        <a href="#" class="right-arrow"><i class="la la-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- /.lc-city-listing -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end explore section -->

    <!-- start tag section -->
    <div class="section lc-explore-tag pd-default">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="lc-section-title">
                        <h1 class="entry-title">Explore By Tag</h1>
                        <div class="shadow-text">Categories</div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing</p>
                        <a class="btn btn-listing" href="#">All Categories</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-6 col-md-3 col-lg-2 lc-single-tag-box wow animated fadeIn" data-wow-duration="0.1s" data-wow-delay="0.1s">
                    <a class="lc-tag-box" href="#">
                        <span class="icon">
                           <img src="images/tag/01.png" alt="img">
                       </span>
                        <span class="title">University</span>
                        <span>3 Listing</span>
                    </a>
                </div>
                <div class="col-6 col-md-3 col-lg-2 lc-single-tag-box wow animated fadeIn" data-wow-duration="0.2s" data-wow-delay="0.2s">
                    <a class="lc-tag-box" href="#">
                        <span class="icon">
                           <img src="images/tag/02.png" alt="img">
                       </span>
                        <span class="title">Dinner Food</span>
                        <span>23 Listing</span>
                    </a>
                </div>
                <div class="col-6 col-md-3 col-lg-2 lc-single-tag-box wow animated fadeIn" data-wow-duration="0.3s" data-wow-delay="0.3s">
                    <a class="lc-tag-box" href="#">
                        <span class="icon">
                           <img src="images/tag/03.png" alt="img">
                       </span>
                        <span class="title">Hospitality</span>
                        <span>4 Listing</span>
                    </a>
                </div>
                <div class="col-6 col-md-3 col-lg-2 lc-single-tag-box wow animated fadeIn" data-wow-duration="0.4s" data-wow-delay="0.4s">
                    <a class="lc-tag-box" href="#">
                        <span class="icon">
                           <img src="images/tag/04.png" alt="img">
                       </span>
                        <span class="title">Car Parking</span>
                        <span>7 Listing</span>
                    </a>
                </div>
                <div class="col-6 col-md-3 col-lg-2 lc-single-tag-box wow animated fadeIn" data-wow-duration="0.5s" data-wow-delay="0.5s">
                    <a class="lc-tag-box" href="#">
                        <span class="icon">
                           <img src="images/tag/05.png" alt="img">
                       </span>
                        <span class="title">Musician</span>
                        <span>5 Listing</span>
                    </a>
                </div>
                <div class="col-6 col-md-3 col-lg-2 lc-single-tag-box wow animated fadeIn" data-wow-duration="0.6s" data-wow-delay="0.6s">
                    <a class="lc-tag-box" href="#">
                        <span class="icon">
                           <img src="images/tag/06.png" alt="img">
                       </span>
                        <span class="title">Night Clab</span>
                        <span>2 Listing</span>
                    </a>
                </div>
                <div class="col-6 col-md-3 col-lg-2 lc-single-tag-box wow animated fadeIn" data-wow-duration="0.7s" data-wow-delay="0.7s">
                    <a class="lc-tag-box" href="#">
                        <span class="icon">
                           <img src="images/tag/07.png" alt="img">
                       </span>
                        <span class="title">Spa</span>
                        <span>2 Listing</span>
                    </a>
                </div>
                <div class="col-6 col-md-3 col-lg-2 lc-single-tag-box wow animated fadeIn" data-wow-duration="0.8s" data-wow-delay="0.8s">
                    <a class="lc-tag-box" href="#">
                        <span class="icon">
                           <img src="images/tag/08.png" alt="img">
                       </span>
                        <span class="title">Shopping</span>
                        <span>15 Listing</span>
                    </a>
                </div>
                <div class="col-6 col-md-3 col-lg-2 lc-single-tag-box wow animated fadeIn" data-wow-duration="0.9s" data-wow-delay="0.9s">
                    <a class="lc-tag-box" href="#">
                        <span class="icon">
                           <img src="images/tag/09.png" alt="img">
                       </span>
                        <span class="title">Coffee & Tea</span>
                        <span>3 Listing</span>
                    </a>
                </div>
                <div class="col-6 col-md-3 col-lg-2 lc-single-tag-box wow animated fadeIn" data-wow-duration="1.0s" data-wow-delay="1.0s">
                    <a class="lc-tag-box" href="#">
                        <span class="icon">
                           <img src="images/tag/10.png" alt="img">
                       </span>
                        <span class="title">Fitness</span>
                        <span>5 Listing</span>
                    </a>
                </div>
                <div class="col-6 col-md-3 col-lg-2 lc-single-tag-box wow animated fadeIn" data-wow-duration="1.1s" data-wow-delay="1.1s">
                    <a class="lc-tag-box" href="#">
                        <span class="icon">
                           <img src="images/tag/11.png" alt="img">
                       </span>
                        <span class="title">Art & History</span>
                        <span>1 Listing</span>
                    </a>
                </div>
                <div class="col-6 col-md-3 col-lg-2 lc-single-tag-box wow animated fadeIn" data-wow-duration="1.2s" data-wow-delay="1.2s">
                    <a class="lc-tag-box" href="#">
                        <span class="icon">
                           <img src="images/tag/12.png" alt="img">
                       </span>
                        <span class="title">Cinema</span>
                        <span>3 Listing</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- end tag section -->

    <!-- start event section -->
    <div class="section lc-explore-event pd-default">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="lc-section-title">
                        <h1 class="entry-title">Explore Events</h1>
                        <div class="shadow-text">Events</div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing</p>
                        <a class="btn btn-listing" href="#">Show More</a>
                    </div>
                </div>
                <!-- event-single -->
                <div class="col-12 col-lg-4 col-md-6 wow animated fadeIn" data-wow-duration="0.1s" data-wow-delay="0.1s">
                    <div class="lc-event-single" data-lat="43.64701" data-lng="-79.39425" data-maptype="gm" data-marker="images/marker/cluster.png">
                        <div class="entry-thumb">
                            <img class="img-thumbnail" src="images/event/01.png" alt="img">
                            <a href="#" class="btn btn-date">Nov 08</a>
                            <div class="entry-thumb-content">
                                <span class="event-time">18:30 - 23:30</span>
                                <span class="float-right">
                                        <i class="fa fa-bookmark-o"></i>
                                        <a class="btn btn-primary" data-toggle="modal" data-target=".lc-listing-modal">
                                           <span><i class="fa fa-eye"></i></span>
                                        </a>
                                    </span>
                            </div>
                        </div>
                        <div class="entry-content">
                            <h4 class="entry-title"><a href="#">The American Revolution</a></h4>
                            <a class="address" href="#"><i class="fa fa-map-marker"></i>3033 Joyce Street, Alabama</a>
                            <div class="author-thumb">
                                <img src="images/author/01.png" alt="img">
                            </div>
                            <h6 class="author-name"><a href="#">Bob Fuentes<small>(Speaker)</small></a></h6>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4 col-md-6 wow animated fadeIn" data-wow-duration="0.2s" data-wow-delay="0.2s">
                    <div class="lc-event-single" data-lat="-37.8210922667" data-lng="175.2209316333" data-maptype="gm" data-marker="images/marker/cluster.png">
                        <div class="entry-thumb">
                            <img class="img-thumbnail" src="images/event/02.png" alt="img">
                            <a href="#" class="btn btn-date">Nov 08</a>
                            <div class="entry-thumb-content">
                                <span class="event-time">18:30 - 23:30</span>
                                <span class="float-right">
                                        <i class="fa fa-bookmark-o"></i>
                                        <a class="btn btn-primary" data-toggle="modal" data-target=".lc-listing-modal">
                                           <span><i class="fa fa-eye"></i></span>
                                        </a>
                                    </span>
                            </div>
                        </div>
                        <div class="entry-content">
                            <h4 class="entry-title"><a href="#">Splendour in the Grass</a></h4>
                            <a class="address" href="#"><i class="fa fa-map-marker"></i>5480 Joyce Street, Oklahama</a>
                            <div class="author-thumb">
                                <img src="images/author/02.png" alt="img">
                            </div>
                            <h6 class="author-name"><a href="#">Beckmann <small>(Speaker)</small></a></h6>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4 col-md-6 wow animated fadeIn" data-wow-duration="0.3s" data-wow-delay="0.3s">
                    <div class="lc-event-single" data-lat="-37.8210819833" data-lng="175.2213903167" data-maptype="gm" data-marker="images/marker/cluster.png">
                        <div class="entry-thumb">
                            <img class="img-thumbnail" src="images/event/03.png" alt="img">
                            <a href="#" class="btn btn-date">Nov 08</a>
                            <div class="entry-thumb-content">
                                <span class="event-time">18:30 - 23:30</span>
                                <span class="float-right">
                                        <i class="fa fa-bookmark-o"></i>
                                        <a class="btn btn-primary" data-toggle="modal" data-target=".lc-listing-modal">
                                           <span><i class="fa fa-eye"></i></span>
                                        </a>
                                    </span>
                            </div>
                        </div>
                        <div class="entry-content">
                            <h4 class="entry-title"><a href="#">Life is Beautiful Festival</a></h4>
                            <a class="address" href="#"><i class="fa fa-map-marker"></i>3033 Joyce Street, Alabama</a>
                            <div class="author-thumb">
                                <img src="images/author/03.png" alt="img">
                            </div>
                            <h6 class="author-name"><a href="#">Jeremy Fuente<small>(Singer)</small></a></h6>
                        </div>
                    </div>
                </div>
                <!-- /.event-single -->
            </div>
        </div>
    </div>
    <!-- end event section -->

    <!-- start call to action -->
    <div class="lc-call-to-action lc-call-to-action-one">
        <div class="container">
            <div class="row">
                <div class="col-4 col-lg-3">
                    <div class="call-thumb">
                        <img src="images/01.png" class="" alt="img">
                    </div>
                </div>
                <div class="col-12 col-lg-8 offset-xl-1">
                    <div class="call-to-action-details">
                        <h2 class="call-title">Want to add your business listing?</h2>
                        <p>Listera will help you to promote your business with nearest communities.</p>
                        <a class="btn btn-blue" href="#">Submit Today</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end call to action -->

    <!-- start feature-author -->
    <div class="section lc-feature-author pd-default">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="lc-section-title">
                        <h1 class="entry-title">Featured Author</h1>
                        <div class="shadow-text">Author</div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing</p>
                        <a class="btn btn-listing" href="#">View Profile</a>
                    </div>
                </div>
                <!-- author-info -->
                <div class="col-lg-6 col-md-12">
                    <div class="lc-author-info">
                        <div class="media">
                            <img class="img-fluid" src="images/author/06.png" alt="image">
                            <div class="media-body">
                                <a class="author-name" href="#">Pascal Beckmann
                                    <span class="rating">
                                        <i class="fa fa-star"></i>4.6
                                        <span class="count">(60)</span>
                                    </span>
                                </a>
                                <p>Lorem ipsum dolor sit amet, consectetur elit sed do eiusmod tempor .</p>
                                <a class="btn btn-blue" href="#">Follow Now</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="row">
                        <!-- author-listing -->
                        <div class="col-12 col-lg-4 col-md-4 wow animated fadeIn" data-wow-duration="0.1s" data-wow-delay="0.1s">
                            <div class="lc-author-listing">
                                <div class="entry-thumb">
                                    <img class="img-thumbnail" src="images/blog/01.png" alt="img">
                                    <div class="entry-thumb-content">
                                        <span class="rating">
                                            <i class="fa fa-star"></i>5.0
                                            <span class="count">(18)</span>
                                        </span>
                                    </div>
                                </div>
                                <div class="entry-content">
                                    <h5 class="title"><a href="#">The Burger House</a></h5>
                                    <a class="address" href="#"><i class="fa fa-map-marker"></i>Hilton, New Jersey</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-4 col-md-4 wow animated fadeIn" data-wow-duration="0.2s" data-wow-delay="0.2s">
                            <div class="lc-author-listing">
                                <div class="entry-thumb">
                                    <img class="img-thumbnail" src="images/blog/02.png" alt="img">
                                    <div class="entry-thumb-content">
                                        <span class="rating">
                                            <i class="fa fa-star"></i>5.0
                                            <span class="count">(18)</span>
                                        </span>
                                    </div>
                                </div>
                                <div class="entry-content">
                                    <h5 class="title"><a href="#">Beautiful Festival</a></h5>
                                    <a class="address" href="#"><i class="fa fa-map-marker"></i>Hilton, New Jersey</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-4 col-md-4 wow animated fadeIn" data-wow-duration="0.3s" data-wow-delay="0.3s">
                            <div class="lc-author-listing">
                                <div class="entry-thumb">
                                    <img class="img-thumbnail" src="images/blog/03.png" alt="img">
                                    <div class="entry-thumb-content">
                                        <span class="rating">
                                            <i class="fa fa-star"></i>5.0
                                            <span class="count">(18)</span>
                                        </span>
                                    </div>
                                </div>
                                <div class="entry-content">
                                    <h5 class="title"><a href="#">The Revolution</a></h5>
                                    <a class="address" href="#"><i class="fa fa-map-marker"></i>Hilton, New Jersey</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.author-listing -->
                    </div>
                </div>
                <!-- /.author-info -->
            </div>
        </div>
    </div>
    <!-- end feature-author -->

    <!-- start news section -->
    <div class="section lc-news-section pd-default">
        <div class="container">
            <div class="row justify-content-center">
                <!-- lc section title -->
                <div class="col-12">
                    <div class="lc-section-title">
                        <h1 class="entry-title">News & Tips</h1>
                        <div class="shadow-text">News</div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing</p>
                        <a class="btn btn-listing" href="#">Show More</a>
                    </div>
                </div>
                <!-- /.lc-section-title -->

                <!-- lc blog single -->
                <div class="col-12 col-md-6 col-lg-4 wow animated fadeIn" data-wow-duration="0.1s" data-wow-delay="0.1s">
                    <div class="lc-blog-single">
                        <div class="entry-thumb">
                            <img class="img-thumbnail" src="images/blog/01.png" alt="img">
                        </div>
                        <div class="entry-content">
                            <a class="title" href="#">Find Some Best Place</a>
                            <ul class="post-info">
                                <li class="admin"><a href="#"><i class="fa fa-user"></i>ThemeLayer</a></li>
                                <li class="date"><i class="fa fa-calendar"></i>25 Nov</li>
                                <li class="commets"><a href="#"><i class="fa fa-tags"></i>Food</a></li>
                            </ul>
                            <p>Lorem ipsum dolor sit amet, consectetur sing  sed do eiusmod tempor aliqua.</p>
                            <a class="btn btn-readmore" href="#">Read More</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 wow animated fadeIn" data-wow-duration="0.2s" data-wow-delay="0.2s">
                    <div class="lc-blog-single">
                        <div class="entry-thumb">
                            <img class="img-thumbnail" src="images/blog/02.png" alt="img">
                        </div>
                        <div class="entry-content">
                            <a class="title" href="#">How Safe Will You Be?</a>
                            <ul class="post-info">
                                <li class="admin"><a href="#"><i class="fa fa-user"></i>ThemeLayer</a></li>
                                <li class="date"><i class="fa fa-calendar"></i>25 Nov</li>
                                <li class="commets"><a href="#"><i class="fa fa-tags"></i>Food</a></li>
                            </ul>
                            <p>Lorem ipsum dolor sit amet, consectetur sing  sed do eiusmod tempor aliqua.</p>
                            <a class="btn btn-readmore" href="#">Read More</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 wow animated fadeIn" data-wow-duration="0.3s" data-wow-delay="0.3s">
                    <div class="lc-blog-single">
                        <div class="entry-thumb">
                            <img class="img-thumbnail" src="images/blog/03.png" alt="img">
                        </div>
                        <div class="entry-content">
                            <a class="title" href="#">Don't Forget Spiritual Life</a>
                            <ul class="post-info">
                                <li class="admin"><a href="#"><i class="fa fa-user"></i>ThemeLayer</a></li>
                                <li class="date"><i class="fa fa-calendar"></i>25 Nov</li>
                                <li class="commets"><a href="#"><i class="fa fa-tags"></i>Health</a></li>
                            </ul>
                            <p>Lorem ipsum dolor sit amet, consectetur sing  sed do eiusmod tempor aliqua.</p>
                            <a class="btn btn-readmore" href="#">Read More</a>
                        </div>
                    </div>
                </div>
                <!-- /.lc-blog-single -->
            </div>
        </div>
    </div>
    <!-- end news section -->

    <!-- start testimonial section -->
    <div class="section lc-testimonial-section pd-top-138">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="lc-section-title text-center">
                        <h1 class="entry-title">People talking about us</h1>
                        <div class="shadow-text">Testimonial</div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing</p>
                    </div>
                </div>
                <div class="col-3 col-lg-3">
                    <div class="author-thumb">
                        <img src="images/author/07.png" alt="img">
                    </div>
                </div>
                <div class="col-12 col-md-10 col-lg-6 offset-lg-2">
                    <div class="lc-testimonial-carousel owl-carousel owl-theme">
                        <div class="item">
                            <div class="testimonial-content">
                                <p>Lorem ipsum dolor sit amet, consctetur adipiscing elit, sed the do eiusmoed tempor indidunt ut labore et dolore magna aliqua. Enim minim to verniam, quis nostrud exercitation ullamco laboris.</p>
                                <div class="media">
                                    <div class="author-thumb">
                                        <img src="images/author/03.png" alt="img">
                                    </div>
                                    <div class="media-body">
                                        <a  href="#">Pascal Beckmann</a>
                                        <span class="author-meta">CEO of Olives</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimonial-content">
                                <p>Lorem ipsum dolor sit amet, consctetur adipiscing elit, sed the do eiusmoed tempor indidunt ut labore et dolore magna aliqua. Enim minim to verniam, quis nostrud exercitation ullamco laboris.</p>
                                <div class="media">
                                    <div class="author-thumb">
                                        <img src="images/author/03.png" alt="img">
                                    </div>
                                    <div class="media-body">
                                        <a  href="#">John Doe</a>
                                        <span class="author-meta">CTO of Olives</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimonial-content">
                                <p>Lorem ipsum dolor sit amet, consctetur adipiscing elit, sed the do eiusmoed tempor indidunt ut labore et dolore magna aliqua. Enim minim to verniam, quis nostrud exercitation ullamco laboris.</p>
                                <div class="media">
                                    <div class="author-thumb">
                                        <img src="images/author/03.png" alt="img">
                                    </div>
                                    <div class="media-body">
                                        <a  href="#">Steve Smith</a>
                                        <span class="author-meta">CEO of Olives</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end slider section -->
@stop
