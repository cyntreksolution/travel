@extends('layouts.master')

@section('content')
    <!-- start page title -->
    <div class="lc-page-title-wrap">
        <div class="lc-page-title text-center lc-page-title-addlisting-bg rellax" data-rellax-speed="-4">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 rellax" data-rellax-speed="3">
                        <h2>Make a Story</h2>
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/">Home</a></li>
                                <li class="breadcrumb-item"><a href="#">Make Story</a></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <!-- start listing details section -->
    <div class="bg-single">
        <div class="lc-container">
            <div class="lc-listing-detaits-section lc-single-page-top100 pb-5">
                <!-- add listing form -->
            {!! Form::open(['route' => 'stories.store','method'=>'POST','files'=>true,'class'=>'pt-5']) !!}


            <!-- add listing details -->
                <div class="lc-add-listing-details ">
                    <div class="lc-section-title lc-section-title-2">
                        <h4 class="entry-title"><i class="fa fa-pencil-square-o"></i>My Story</h4>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <label class="lc-add-listing-wrap">
                                <span class="lc-add-listing-input-title">Title <span>*</span></span>
                                <input class="lc-add-listing-input cpdl-map"
                                       placeholder="Example: Ella Rock With Buddies"
                                       type="text">
                            </label>
                        </div>
                        <div class="col-lg-4">
                            <label class="lc-add-listing-wrap">
                                <span class="lc-add-listing-input-title">Location <span>*</span> </span>
                                <input class="lc-add-listing-input" id="autocomplete_search"
                                       placeholder="Example: Ella Rock"
                                       type="text">
                            </label>
                        </div>
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-12">
                                    <label class="lc-add-listing-wrap">
                                        <span class="lc-add-listing-input-title">Story <span>*</span></span>
                                        <textarea class="lc-add-listing-input" name="story"
                                                  placeholder="Example: Take a full-day tour from Ella to visit Udawalawe National Park. Discover a wide array of wildlife including elephants, buffalo, deer and crocodiles."></textarea>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="lc-multiple-img-upload p-3">
                    <div class="lc-section-title lc-section-title-2">
                        <h4 class="entry-title"><i class="fa fa-picture-o"></i>Gallery <span>*</span></h4>
                    </div>

                    <div>
                        <span class="lc-add-listing-input-title">Gallery Image (maximum 4 images only)</span>
                        <input type="file" class="form-control" name="photo[]" id="photo" accept=".png, .jpg, .jpeg"
                               onchange="readFile(this);" multiple>
                        <div id="status"></div>
                        <div id="photos" class="row"></div>
                    </div>
                </div>

                <input type="hidden" name="recaptcha" id="recaptcha" value="">
                <input type="hidden" name="lat" id="lat" value="">
                <input type="hidden" name="long" id="long" value="">
                <input type="hidden" name="place" id="place" value="">

                <div class="lc-add-listing-form-btn">
                    <button type="submit" class="btn btn-blue" href="#">Save & Publish</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>


    <!-- end listing details section -->
@endsection

@push('styles')
    <link href="{{ asset('css/jodit.min.css') }}" rel="stylesheet">
    <style>

    </style>
@endpush

@push('scripts')
    <script type="text/javascript" src="{{ asset('js/jodit.min.js') }}"></script>

    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjIeWWh_hSnTYKMLul93oodx4f_lGTKac &amp;libraries=places"></script>

    <script>
        google.maps.event.addDomListener(window, 'load', initialize);

        function initialize() {
            var input = document.getElementById('autocomplete_search');
            var options = {
                // types: ['geocode'],  // or '(cities)' if that's what you want?
                componentRestrictions: {country: "LK"}
            };
            var autocomplete = new google.maps.places.Autocomplete(input, options);
            autocomplete.addListener('place_changed', function () {
                var place = autocomplete.getPlace();
                console.log(place);
                $('#lat').val(place.geometry['location'].lat());
                $('#long').val(place.geometry['location'].lng());
                $('#place').val(place.formatted_address);
            });
        }
    </script>

    <script>
        $('textarea').each(function () {
            let editor = new Jodit(this, {
                removeButtons: [
                    "ol",
                    "about",
                    "format",
                    "fullsize",
                    ""
                ]
            });
        });
    </script>

    <script>
        function readFile(input) {
            $("#status").html('Processing...');
            counter = input.files.length;
            for (x = 0; x < counter; x++) {
                if (input.files && input.files[x]) {

                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $("#photos").append('<div class="col-md-3 col-sm-3 col-xs-3"><img src="' + e.target.result + '" class="img-thumbnail"></div>');
                    };

                    reader.readAsDataURL(input.files[x]);
                }
            }
            if (counter == x) {
                $("#status").html('');
            }
        }
    </script>

    <script src="https://www.google.com/recaptcha/api.js?render=6Lf0b8AUAAAAAOYkLuS6HD822q_QcoznRtpNI-g-"></script>
    <script>
        grecaptcha.ready(function() {
            grecaptcha.execute('{{ env('CAPTCHA_SITE_KEY') }}', {action: 'homepage'}).then(function(token) {
                if (token){
                  document.getElementById('recaptcha').value = token;
              }
            });
        });
    </script>
@endpush
