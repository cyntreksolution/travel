@extends('layouts.master')

@section('content')
    <!-- start page title -->
    <div class="lc-page-title-wrap">
        <div class="lc-page-title text-center lc-page-title-addlisting-bg rellax" data-rellax-speed="-4">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 rellax" data-rellax-speed="3">
                        <h2>Add Package</h2>
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/">Home</a></li>
                                <li class="breadcrumb-item"><a href="#">Add Package</a></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <!-- start listing details section -->
    <div class="bg-single">
        <div class="lc-container">
            <div class="lc-listing-detaits-section lc-single-page-top100 pb-5">
                <!-- add listing form -->
            {!! Form::open(['route' => 'packages.store','method'=>'POST','files'=>true,'class'=>'pt-5']) !!}


            <!-- add listing details -->
                <div class="lc-add-listing-details ">
                    <div class="lc-section-title lc-section-title-2">
                        <h4 class="entry-title"><i class="fa fa-pencil-square-o"></i>Package Details</h4>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <label class="lc-add-listing-wrap">
                                <span class="lc-add-listing-input-title">Title <span>*</span></span>
                                <input class="lc-add-listing-input cpdl-map"
                                       placeholder="Example: Ella Rock 2 Day Tour"
                                       type="text">
                            </label>
                        </div>
                        <div class="col-lg-6 col-12">
                            <div class="lc-select-input-field">
                                <span class="lc-add-listing-input-title">Category <span>*</span></span>
                                {!! Form::select('category', $categories, null, ['placeholder' => 'Example: Hiking']) !!}
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label class="lc-add-listing-wrap">
                                        <span class="lc-add-listing-input-title">Description <span>*</span></span>
                                        <textarea class="lc-add-listing-input" name="description"
                                                  placeholder="Example: Take a full-day tour from Ella to visit Udawalawe National Park. Discover a wide array of wildlife including elephants, buffalo, deer and crocodiles."></textarea>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label class="lc-add-listing-wrap">
                                        <span class="lc-add-listing-input-title">Meet point</span>
                                        <input class="lc-add-listing-input" name="meet_point"
                                               placeholder="Example: Bandaranayaka International Airport"
                                               type="text">
                                    </label>
                                </div>
                                <div class="col-lg-4">
                                    <label class="lc-add-listing-wrap">
                                        <span class="lc-add-listing-input-title">Meet Day </span>
                                        <input class="lc-add-listing-input" name="meet_point"
                                               placeholder="Example: 10/12/2019"
                                               type="date">
                                    </label>
                                </div>
                                <div class="col-lg-4">
                                    <label class="lc-add-listing-wrap">
                                        <span class="lc-add-listing-input-title">Meet Time</span>
                                        <input class="lc-add-listing-input" name="meet_time"
                                               placeholder="Example: 10:00"
                                               type="time">
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label class="lc-add-listing-wrap">
                                        <span class="lc-add-listing-input-title">Departure point </span>
                                        <input class="lc-add-listing-input" name="departure_point"
                                               placeholder="Example: Bandaranayaka International Airport"
                                               type="text">
                                    </label>
                                </div>
                                <div class="col-lg-4">
                                    <label class="lc-add-listing-wrap">
                                        <span class="lc-add-listing-input-title">Departure Day </span>
                                        <input class="lc-add-listing-input" name="departure_point"
                                               placeholder="Example: 10/12/2019"
                                               type="date">
                                    </label>
                                </div>
                                <div class="col-lg-4">
                                    <label class="lc-add-listing-wrap">
                                        <span class="lc-add-listing-input-title">Departure Time</span>
                                        <input class="lc-add-listing-input" name="departure_time"
                                               placeholder="Example: 10:00"
                                               type="time">
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label class="lc-add-listing-wrap">
                                        <span class="lc-add-listing-input-title">Minimum Persons </span>
                                        <input class="lc-add-listing-input" name="minimum_person"
                                               placeholder="Example: 1"
                                               type="number">
                                    </label>
                                </div>
                                <div class="col-lg-4">
                                    <label class="lc-add-listing-wrap">
                                        <span class="lc-add-listing-input-title">Maximum Persons </span>
                                        <input class="lc-add-listing-input" name="maximum_person"
                                               placeholder="Example: 10"
                                               type="text">
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label class="lc-add-listing-wrap">
                                        <span class="lc-add-listing-input-title">Distance </span>
                                        <input class="lc-add-listing-input" name="distance" placeholder="Example: 80 km"
                                               type="text">
                                    </label>
                                </div>
                                <div class="col-lg-4">
                                    <label class="lc-add-listing-wrap">
                                        <span class="lc-add-listing-input-title">Duration <span>*</span> </span>
                                        <input class="lc-add-listing-input" name="duration"
                                               placeholder="Example: 07 - 08 Hours"
                                               type="text">
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label class="lc-add-listing-wrap">
                                        <span class="lc-add-listing-input-title">Primary Location <span>*</span> </span>
                                        <input class="lc-add-listing-input" id="autocomplete_search"
                                               placeholder="Example: Ella Rock"
                                               type="text">
                                    </label>
                                </div>

                                <div class="col-lg-4">
                                    <div class="lc-select-input-field">
                                        <span class="lc-add-listing-input-title">Nearest City <span>*</span> </span>
                                        {!! Form::select('city_id', $cities, null, ['placeholder' => 'Example: Ella']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label class="lc-add-listing-wrap">
                                        <span class="lc-add-listing-input-title">Includes <span>*</span></span>
                                        <textarea class="lc-add-listing-input" name="includes"
                                                  placeholder="Example:<br>Pick-up from the hotel<br>Travel-insurance for each person "></textarea>
                                    </label>
                                </div>
                                <div class="col-lg-6">
                                    <label class="lc-add-listing-wrap">
                                        <span class="lc-add-listing-input-title">Excludes <span>*</span></span>
                                        <textarea class="lc-add-listing-input" name="excludes"
                                                  placeholder="Example:<br>Personal Expenses<br>Entrance tickets "></textarea>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-12">
                            <div class="lc-select-input-field">
                                <span class="lc-add-listing-input-title">Price Range</span>
                                {!! Form::select('price_range', ['Budget'=>'Budget','Normal'=>'Normal','Luxury'=>'Luxury'], null, ['placeholder' => 'Example: Normal']) !!}
                            </div>
                        </div>
                        <div class="col-lg-4 col-12">
                            <div class="lc-select-input-field">
                                <span class="lc-add-listing-input-title">Price From</span>
                                <input class="lc-add-listing-input" name="price_from" placeholder="Example: 2500 LKR"
                                       type="text">
                            </div>
                        </div>
                        <div class="col-lg-4 col-12">
                            <div class="lc-select-input-field">
                                <span class="lc-add-listing-input-title">Price To</span>
                                <input class="lc-add-listing-input" name="price_from" placeholder="Example: 8500 LKR"
                                       type="text">
                            </div>
                        </div>
                    </div>
                </div>


                <!-- add Business Information -->
            {{--                    <div class="lc-business-info">--}}
            {{--                        <div class="lc-section-title lc-section-title-2">--}}
            {{--                            <h4 class="entry-title"><i class="fa fa-address-card-o"></i> Package Days</h4>--}}
            {{--                        </div>--}}
            {{--                        <div class="row">--}}
            {{--                            <!-- business hour -->--}}
            {{--                            <div class="col-12">--}}
            {{--                                <div class="lc-business-hour">--}}
            {{--                                    <div class="row">--}}

            {{--                                        <div class="col-lg-11 col-md-10 col-12">--}}
            {{--                                            <div class="row">--}}
            {{--                                                <!-- day -->--}}
            {{--                                                <div class="col-md-4 col-12">--}}
            {{--                                                    <label class="lc-add-listing-wrap">--}}
            {{--                                                        <span class="lc-add-listing-input-title">Day Title</span>--}}
            {{--                                                        <input class="lc-add-listing-input"--}}
            {{--                                                               placeholder="Your Contact Number"--}}
            {{--                                                               type="text">--}}
            {{--                                                    </label>--}}
            {{--                                                </div>--}}
            {{--                                                <!-- start time -->--}}
            {{--                                                <div class="col-md-4 col-12">--}}
            {{--                                                    <label class="lc-add-listing-wrap">--}}
            {{--                                                        <span class="lc-add-listing-input-title">Day Description</span>--}}
            {{--                                                        <textarea class="lc-add-listing-input"></textarea>--}}
            {{--                                                    </label>--}}
            {{--                                                </div>--}}
            {{--                                            </div>--}}
            {{--                                        </div>--}}
            {{--                                        <!-- add new handler -->--}}
            {{--                                        <div class="col-lg-1 col-md-2 col-12">--}}
            {{--                                            <a class="lc-add-new-business-hour-handler"><i--}}
            {{--                                                    class="fa fa-plus"></i></a>--}}
            {{--                                        </div>--}}
            {{--                                    </div>--}}
            {{--                                </div>--}}
            {{--                            </div>--}}
            {{--                        </div>--}}
            {{--                    </div>--}}

            <!-- add listing Gallery -->
                <div class="lc-multiple-img-upload p-3">
                    <div class="lc-section-title lc-section-title-2">
                        <h4 class="entry-title"><i class="fa fa-picture-o"></i>Gallery <span>*</span></h4>
                    </div>

                    <div>
                        <span class="lc-add-listing-input-title">Gallery Image (maximum 4 images only)</span>
                        <input type="file" class="form-control" name="photo[]" id="photo" accept=".png, .jpg, .jpeg"
                               onchange="readFile(this);" multiple>
                        <div id="status"></div>
                        <div id="photos" class="row"></div>
                    </div>
                </div>
                <input type="hidden" name="lat" id="lat" value="">
                <input type="hidden" name="long" id="long" value="">
                <input type="hidden" name="place" id="place" value="">
                <input type="hidden" name="recaptcha" id="recaptcha" value="">

                <div class="lc-add-listing-form-btn">
                    <button type="submit" class="btn btn-blue" href="#">Save & Submit for Review</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>


    <!-- end listing details section -->
@endsection

@push('styles')
    <link href="{{ asset('css/jodit.min.css') }}" rel="stylesheet">
    <style>

    </style>
@endpush

@push('scripts')
    <script type="text/javascript" src="{{ asset('js/jodit.min.js') }}"></script>

    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjIeWWh_hSnTYKMLul93oodx4f_lGTKac &amp;libraries=places"></script>

    <script>
        google.maps.event.addDomListener(window, 'load', initialize);

        function initialize() {
            var input = document.getElementById('autocomplete_search');
            var options = {
                // types: ['geocode'],  // or '(cities)' if that's what you want?
                componentRestrictions: {country: "LK"}
            };
            var autocomplete = new google.maps.places.Autocomplete(input, options);
            autocomplete.addListener('place_changed', function () {
                var place = autocomplete.getPlace();
                console.log(place);
                $('#lat').val(place.geometry['location'].lat());
                $('#long').val(place.geometry['location'].lng());
            });
        }
    </script>

    <script>
        $('textarea').each(function () {
            let editor = new Jodit(this, {
                removeButtons: [
                    "image",
                    "ol",
                    "about",
                    "format",
                    "fullsize",
                    ""
                ]
            });
        });
    </script>

    <script>
        function readFile(input) {
            $("#status").html('Processing...');
            counter = input.files.length;
            for (x = 0; x < counter; x++) {
                if (input.files && input.files[x]) {

                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $("#photos").append('<div class="col-md-3 col-sm-3 col-xs-3"><img src="' + e.target.result + '" class="img-thumbnail"></div>');
                    };

                    reader.readAsDataURL(input.files[x]);
                }
            }
            if (counter == x) {
                $("#status").html('');
            }
        }
    </script>

    <script src="https://www.google.com/recaptcha/api.js?render=6Lf0b8AUAAAAAOYkLuS6HD822q_QcoznRtpNI-g-"></script>
    <script>
        grecaptcha.ready(function() {
            grecaptcha.execute('{{ env('CAPTCHA_SITE_KEY') }}', {action: 'homepage'}).then(function(token) {
                if (token){
                  document.getElementById('recaptcha').value = token;
              }
            });
        });
    </script>
@endpush
