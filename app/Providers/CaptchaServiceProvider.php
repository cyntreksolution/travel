<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class CaptchaServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('recaptcha', function ($field, $token, $parameters, $validator) {
            $captcha= $token;
            $url = 'https://www.google.com/recaptcha/api/siteverify';
            $data = array('secret' => env('CAPTCHA_SECRET_KEY'), 'response' => $captcha);

            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data)
                )
            );

            try{
                $context  = stream_context_create($options);
                $response = file_get_contents($url, false, $context);
                $resultJson = json_decode($response,true);
                header('Content-type: application/json');
                if($resultJson['success']) {
                    return true;
                } else {
                    return false;
                }
            }catch (\Exception $exception){
                dd($exception);
            }
        });
    }
}
