<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('category_id')->nullable(true)->unsigned();
            $table->bigInteger('user_id')->nullable(true)->unsigned();
            $table->bigInteger('city_id');

            $table->string('name', 300)->nullable(true);
            $table->text('image')->nullable(true);
            $table->text('description')->nullable(true);
            $table->decimal('rating', 8, 2)->nullable(true)->default(5);

            $table->tinyInteger('status')->default(0); // 0 = pending ,1 = approve ,2 = need to edit ,3 = rejected

            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('package_descriptions', function (Blueprint $table) {
            $table->bigInteger('id')->autoIncrement()->unsigned();
            $table->bigInteger('package_id')->nullable(true)->unsigned();
            $table->text('meet_point')->nullable(true);
            $table->date('meet_date')->nullable(true);
            $table->time('meet_time')->nullable(true);
            $table->text('departure_point')->nullable(true);
            $table->date('departure_date')->nullable(true);
            $table->time('departure_time')->nullable(true);
            $table->integer('min_person')->nullable(true);
            $table->integer('max_person')->nullable(true);
            $table->text('languages')->nullable(true);
            $table->text('includes')->nullable(true);
            $table->text('excludes')->nullable(true);
            $table->text('popular_places')->nullable(true);
            $table->text('other')->nullable(true);
            $table->text('place')->nullable(true);
            $table->text('map_data')->nullable(true);
            $table->text('price_range')->nullable(true);
            $table->float('price_from')->nullable(true);
            $table->float('price_to')->nullable(true);

            $table->foreign('package_id')->references('id')->on('packages')->onDelete('cascade')->onUpdate('cascade');


            $table->softDeletes();
            $table->timestamps();

        });

        Schema::create('package_images', function (Blueprint $table) {
            $table->bigInteger('id')->unsigned();
            $table->bigInteger('package_id')->nullable(true)->unsigned();
            $table->string('title', 300)->nullable(true);
            $table->text('description')->nullable(true);
            $table->text('file_path')->nullable(true);
            $table->text('file_name')->nullable(true);
            $table->integer('order')->nullable(true);

            $table->foreign('package_id')->references('id')->on('packages')->onDelete('cascade')->onUpdate('cascade');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
        Schema::dropIfExists('package_descriptions');
        Schema::dropIfExists('package_images');
    }
}
